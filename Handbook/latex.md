

# Latex

## Basic template

> main.tex:

```latex
\documentclass[12pt,a4paper]{article}    % document setup
\usepackage[ngerman]{babel}              % set document language to german
\usepackage{graphicx}                    % for images
\usepackage{csquotes}

% Bibliography setup
\usepackage[style=apa,backend=biber, language=german]{biblatex}
\DeclareLanguageMapping{american}{american-apa}
\addbibresource{refs.bib}

\begin{document}


\input{titlepage}						% include another .tex document
\input{abstract}                        % include yet another .tex 

\newpage

\tableofcontents
\newpage

\section{section 1}

\section{Section 2}

\listoffigures
\listoftables
\printbibliography

\appendix

\section{Appendix 1}
\section{Appendix 2}


\end{document}
```

The appendix numbering can be changed to roman by inserting this line right 
before `\appendix`. This changes the section Numbering for all following sections.

```latex
\renewcommand\thesection{\Roman}
```


> titlepage.tex:

```latex
\begin{titlepage}
    \begin{center}
        \vspace*{1cm}				% * to tell latex not to ignore this

        \Huge
        \textbf{Hyperloop statt Flugverkehr}\\
        \vspace{0.5cm}
        \Large{Analyse der Hyperlooptechnologie als nachhaltige 
                Alternative zu Kurzstreckenflügen}
        
        \vspace{1cm}

        \large
        Bachelorarbeit \\
        \vfill						% push text below to page bottom

        \textbf{Lucas Aebi}\\
        \normalsize
        Ingenierusinformatik\\

        \vspace{0.5cm}

        OST - Ostschweizer Fachhochschule\\ 
        Institut für Transport und Verkehr\\



    \end{center}
\end{titlepage}

```

## Automatic recompiling

The following command automatically recompiles paper.tex on every save. 
It compiles as many times as needed to generate all references.

```bash
latexmk -pdf -pvc paper.tex
```

The vscode extension "LaTeX Workshop" will also automatically compile the project on 
every save.

## Latex text sizes 

- \\Huge
- \\huge
- \\LARGE
- \\Large
- \\large
- \\normalsize
- \\small
- \\footnotesize
- \\scriptsize
- \\tiny

