

# SSH

## Tools 

- ssh
- ssh-keygen
- ssh-copy-id

## Info

To use ssh without the need to enter a password on every connect create an ssh-key:

```bash
ssh-keygen -t rsa -C <comment>
```
Leave the passphrase empty, otherwise ssh will ask for the passphrase everytime the 
key gets used. The key will be stored in `.ssh/id_rsa` and `.ssh/id_rsa.pub`.

Then install the key on the target system:

```bash
ssh-copy-id username@server
```

Logging in via ssh will not ask for a passowrd anymore. 

The generated key can also be used for git. The user can upload their public key 
on gitlab/github and clone a repository with ssh. This way no password manager/store 
is needed for git and git will never ask for login credentials.

## Tipps 

Make an alias in your `.bashrc` or `.zshrc` to start an ssh session: 
`alias server="ssh user@server"`

