

# Display

## Tools

- xrandr
- autorandr
- arandr
- lxrandr

## Info

`xrandr` is a tool to configure displays via command line. `arandr` and 
`lxrandr` are GUI tools built on top of `xrandr`. Both can save and 
load configuration files which are just bash scripts calling 
`xrandr`. 

`autorandr` can save and load profiles and also detect automatically 
which profile fits to the current display setup.
