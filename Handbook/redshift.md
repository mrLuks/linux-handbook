
# Redshift

## Tools

- redshift
- redshift-gtk

## Info

`Redshift` is an application that ajusts the display's color temperature 
depending to the time of day. `redshift-gtk` is a system-tray applet for 
`redshift`. Both of these can be configured in `.config/redshift.conf`.

> ~/.config/redshift.conf

```ini
  [redshift]
   temp-day=5700
   temp-night=3600
   dawn-time=06:30
   dusk-time=19:00
   gamma=0.8
   
   [manual]
   lat=55.7
   lon=12.6
```

To set a permanent temperature that is not changing during the day 
one can set the properties `temt-day` and `temp-night` to the same 
value or set the temperature via command line:

```bash
	redshift -P -O 5000
```

This command can be put inside a window manager config file or set as 
a startup command to automatically apply the setting on login.

