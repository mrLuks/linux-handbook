
# Touchpad Configuration

## Tools

- xinput

## Info

Set touchpad accel speed and enable tap-to-click. 
**Warning** Errors in xorg config files can cause an unbootable system!

> /etc/X11/xorg.conf.d

```conf
Section "InputClass"
  Identifier "touchpad"
  Driver "libinput"
  MatchIsTouchpad "on"
  Option "Tapping" "on"
  Option "AccelSpeed" "0.6"
EndSection
```

## Resources

- man xorg.conf
- man libinput
- [Arch wiki libinput](https://wiki.archlinux.org/title/Libinput)
- [touchpad with libinput](https://forums.freebsd.org/threads/touchpad-setup-with-libinput.79889/)

