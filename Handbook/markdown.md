

# Markdown

## Tools

- pandoc
- wkhtmltopdf
- pdflatex

## Info

Pandoc can convert Markdown to various other formats using tools such as 
latex or wkhtmltopdf.

The following script converts a .md file to a pdf using pandoc and wkhtmltopdf. 
It highlights code and applies a custom css stylesheet if a file "style.css" is 
in the same directory as the .md file. Otherwise a default css will be applied. 
The script should be stored in 
the mentioned directory which must also contain a css file calles "pdfstyle.css".
If the scripts folder is in $PATH a file can be convertet with: `mdpdf myfile.md`

> $HOME/scripts/mdpdf

```bash
#!/usr/bin/env bash

MDNAME=$1
name=${MDNAME%.*}
stylesheet="$HOME/scripts/pdfstyle.css"
#stylesheet="style.css"

if [[ -f style.css ]]; then
	stylesheet="style.css"
fi
		
pandoc $name.md -s  -o $name.pdf -t html5 --mathml --css $stylesheet --highlight-style=tango

#########################################
# -s for standalone
# -t html5 for using wkhtmltopdf
# --mathml for embedding latex in html
# --css ... provides a css stylesheet
```

This following script works similar to the before mentioned one but takes every .md 
file in the given directory as input and creates a collective pdf. The name of the 
pdf will be the same as the containing directory.

> $HOME/scripts/mdpdfall

```bash
#!/usr/bin/env bash

dirname=$(pwd)
dirname=$(basename $dirname)

stylesheet=$HOME/scripts/pdfstyle.css

if [ -f style.css ]; then
		stylesheet=style.css
fi

cat *.md | pandoc -o $dirname.pdf -t html5 --mathml --css $stylesheet --highlight-style=tango
```
This stylesheet was used to create a pdf of this document:

> styles.css 

```css
body {
  color: #444;
  font-family: monospace, Georgia, Palatino, 'Palatino Linotype', Times, 'Times New Roman', serif;
  font-size: 12px;
  line-height: 1.7;
  padding: 1em;
  margin: auto;
  max-width: 100%;
  background: #fefefe;
}
h1, h2, h3, h4, h5, h6 {
  color: #111;
  color: #268BD2;
  line-height: 100%;
  margin-top: 1em;
  font-weight: normal;
}

h4, h5, h6 {
  font-weight: bold;
}

h1 {
  font-size: 2.5em;
  border-bottom: #111 solid 1px;
  page-break-before: always;
}
h1:first-of-type {
  page-break-before: unset;
  page-break-before: avoid;
}

h2 {
  font-size: 2em;
}

h3 {
  font-size: 1.5em;
}

h4 {
  font-size: 1.2em;
}

h5 {
  font-size: 1em;
}

h6 {
  font-size: 0.9em;
}  h1, h2, h3 {
    page-break-after: avoid;
}

blockquote {
  color: #666666;
  margin: 0;
  padding-left: 3em;
  border-left: 0.5em #EEE solid;
}
a {
  color: rgb(226, 148, 4);
  color: #0645ad;
  text-decoration: none;
}
a[href]:after {
  content: " (" attr(href) ")";
  color: #0645ad;
  font-size: 9px;
}
p {
  margin: 1em 0;
}
li {
  list-style-type: none;
}
li:before { 
  margin-left: -15px;
  margin-right: 5px;
  content: "=";
  color: rgb(226, 148, 4);
};

img {
  max-width: 100%;
}
code {
    color: #000;
    padding: 1px 4px;
    background-color: #EEE;
    border-radius: 5px;
}
.sourceCode code {
    background-color:unset;
}
```

## Tipps

When multiple md files get concatenated, always put two blank lines at the beginnig of each 
md file. Otherwise the first line may gets merged with the last line of another document. 
Always do this when using the script `mdpdfall` from above.

## Resources 

- [Pandoc User's Guide](https://pandoc.org/MANUAL.html)

