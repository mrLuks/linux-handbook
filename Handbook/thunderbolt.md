

# Tunderbolt

## Tools 

- bolt: thunderbolt daemon thunderbolt daemonthunderbolt daemonthunderbolt daemonthunderbolt daemon
- boltctl
- tbtadm (intel)

## Info

Thunderbolt may needs to be configured in BIOS. 
BIOS-Assist-Mode should be turned off if kernel is 4.19+.

Thunderbolt security is set in the BIOS and should be set to userspace. With 
this setting a new device must be enrolled by the user for example with 
`boltctl enroll <uuid>`



## Resources

- [Lenovo.com BIOS-Assist-mode](https://forums.lenovo.com/t5/Other-Linux-Discussions/Thunderbolt-BIOS-Assist-Mode-clearification-needed/td-p/4559399)
