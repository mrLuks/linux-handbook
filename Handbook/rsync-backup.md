

# Rsync Backup

## Tools

- ssh
- rsync

## Info

The following scripts can create a backup of a provided file/folder on a remote 
location via ssh or restore a file/folder on the local pc from the backup server. 

These scripts may not be perfect but somehow do the job.

> backup
```bash
#!/usr/bin/env bash

### config ###############################
# <username>@<serveraddress>:
sshServer=lucas@mediaserver.local

# Basepath to backup directory on server
base="/home/lucas/backups/ThinkPad-P1/"

##########################################


path=$(realpath --strip $1) &&
echo $path

if [ -e $path ]; then
	newpath=$base$(realpath --relative-to=$HOME --no-symlinks $path)
	if [ -d $path ]; then
			path=$path/
			newpath=$newpath/
	fi
	echo "backing up $newpath"
	rsync -avsR --delete $path $sshServer:$base
	#rsync -rvsR --delete $path $sshServer:$base
else
	echo "Invalid path"
fi
```
> backup-restore

```bash
#!/usr/bin/env bash

### config ###############################
# <username>@<serveraddress>:
sshServer=lucas@mediaserver.local

# Basepath to backup directory on server
#base="/home/lucas/backups/MacBookPro"
base="/home/lucas/backups/ThinkPad-P1"

##########################################

path=$(realpath --strip $1) &&
echo $path

	newpath=$base$(realpath --no-symlinks $path)
	if [ -d $path ]; then
			path=$path
			newpath=$newpath/
	fi
	echo "newpath: $newpath"
	echo "path $path"
	echo "base $base"
	rsync -avs  $sshServer:$newpath $path
	#rsync -rvsR --delete $path $sshServer:$base
```

## Tipps

When using the above scripts with ssh make sure you have setup ssh to work with ssh-keys.
This way the scripts can be run without triggering a password promt. That allows to run 
the scripts automatically with cron or other tools.
See the chapter SSH for more info on ssh-keys.


