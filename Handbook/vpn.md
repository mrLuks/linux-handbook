
# VPN

## Tools & packages

- nm-applet
- network-manager
- network-manager-openconnect-gnome
- network-manager-openvpn-gnome
- strongswan-nm

## Cisco Anyconnect

One option is to use Cisco's official client, but it is much more convenient 
to use 'network-manager' to connect to the Cisco Anyconnect vpn. The package 
'netowrk-manager-openconnect-gnome' adds the required functionality.

Using 'nm-applet' click on "Edit Connections...". This opens a window that 
lists all network connections of your system. Click the "+" on the bottom 
left and choose "Cisco AnyConnect or openconnect", enter the server address 
and your login credentials and done. 

The VPN connection will now appear in the vpn section of 'nm-applet'.

## Protonvpn

Protonvpn offers an official linux client(GUI and CLI). There is nothing wrong with it. 
However, when connected to protonvpn with the official app, local LAN 
connections to a Raspberry Pi or Samba share are not possible anymore. 

Luckily, Protonvpn offers openvpn/IKEV2 configuration files and credentials 
which can be used with for example 'network-manager' and the package 
'network-manager-openvpn-gnome' for openvpn or 'strongswan-nm' for IKEv2. 
When adding a new vpn connection with 
'nm-applet', choose the Connection Type "Import a saved VPN configuration..." 
for using an openvpn config file or the type "IPsec/IKEv2". 

With openvpn (maybe IKEv2 also) connections to local devices are will 
not be blocked by protonvpn.

